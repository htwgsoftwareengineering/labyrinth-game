/**
 * This package contains the Labyrinth-Game which is based on the idea
 * of Ravensburgs "Das verrueckte Labyrinth".
 */
package de.htwg.labyrinth;

import java.util.Scanner;

import de.htwg.labyrinth.controller.LabyrinthController;
import de.htwg.labyrinth.gui.LabyrinthFrame;
import de.htwg.labyrinth.models.Grid;
import de.htwg.labyrinth.models.GridCreateStrategyTemplate;
import de.htwg.labyrinth.tui.TextUI;

/**
 * Main class containing the whole game.
 */
public class Labyrinth {
	private Grid grid;
	
    public Labyrinth(GridCreateStrategyTemplate createStrategy, int x) {
		grid = new Grid(createStrategy, x);
	}

    static Scanner scanner;
	String line = "";
	/**
	 * Main method to start programme.
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		LabyrinthController controller = new LabyrinthController();
		new LabyrinthFrame(controller);
		TextUI tui = new TextUI(controller);
		tui.printTUI();
		// continue until the user decides to quit
		boolean continu = true;
		System.out.println("Please enter number of Players (2-4):");
		scanner = new Scanner(System.in);
		while (continu) {
			continu = tui.processInputLine(scanner.next());
		}
	}
}
