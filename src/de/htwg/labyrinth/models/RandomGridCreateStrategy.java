package de.htwg.labyrinth.models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import de.htwg.util.observer.IObserver;

public class RandomGridCreateStrategy extends GridCreateStrategyTemplate {
	private static final int NUMBER_OF_CARD_IMAGES = 10;
	private static final int NUMBER_OF_TARGETS = 8;
	private static final int NUMBER_OF_DIRECTIONS = 4;
	private static final int MIN_NUMBER_OF_DIRECTIONS = 2;
	private static final int MAX_NUMBER_OF_DIRECTIONS = 3;
	
	private int currentTargetNumber = 0;
	IObserver observer;
	
	RandomGridCreateStrategy(IObserver observ) {
		this.observer = observ;
	}
	
	@Override
	public void fill() {
		for (int row = 1; row <= grid.getSize(); row++) {
			for (int col = 1; col <= grid.getSize(); col++) {
				Card card = getNextCard();
				grid.setCell(card, col, row);
			}
		}
		initCorners();
		spreadTargetsOnGrid();
	}
	
	private Card getCornerCard(int number) throws Exception {
		int pictureNumber;
		switch (number) {
		case 0:
			pictureNumber = 8;
			break;
		case 1:
			pictureNumber = 9;
			break;
		case 2:
			pictureNumber = 6;
			break;
		case 3:
			pictureNumber = 7;
			break;
		default:
			Exception up = new IllegalArgumentException("Number must be in range 1-4! " +  number + " given.");
			// I'm feeling sick
			throw up;
		}
		return getNextCard(pictureNumber);
	}
	
	private void initCorners() {
		for (int i = 0; i < 4; i++) {
			Card card = null;
			try {
				card = getCornerCard(i);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			
			int x = 0;
			int y = 0;
			switch (i) {
			case 0:
				x = 1;
				y = 1;
				break;
			case 1:
				x = grid.getSize();
				y = 1;
				break;
			case 2:
				x = grid.getSize();
				y = grid.getSize();
				break;
			case 3:
				x = 1;
				y = grid.getSize();
				break;
			}
			grid.setCell(card, x, y);
		}
	}
	
	public boolean[] generateWays() {
		Random random = new Random();
		boolean[] ways = new boolean[NUMBER_OF_DIRECTIONS];
		int numberOfTrues = random.nextInt(MAX_NUMBER_OF_DIRECTIONS - MIN_NUMBER_OF_DIRECTIONS + 1);
		numberOfTrues += MIN_NUMBER_OF_DIRECTIONS;
		for (int i = 0; i < NUMBER_OF_DIRECTIONS; i++) {
			ways[i] = false;
		}
		for (int i = 0; i < numberOfTrues; i++) {
			int direction = random.nextInt(NUMBER_OF_DIRECTIONS);
			ways[direction] = true;
		}
		return ways;
	}
	
	private String getAscii(String path) {
		FileReader fr = null;
		try {
			fr = new FileReader(path);
		} catch (FileNotFoundException e) {
			System.out.println("Could not open card ASCII image " + e);
			System.exit(0);
		}
		BufferedReader br = new BufferedReader(fr);
		try {
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				line = br.readLine();
			} catch (IOException e) {
				System.out.println("Could not read line from file " + e);
				System.exit(0);
			}
			while (line != null) {
				sb.append(line);
				sb.append("\n");
				try {
					line = br.readLine();
				} catch (IOException e) {
					System.out.println("Could not read line from file " + e);
					System.exit(0);
				}
			}
			return sb.toString();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("Could not close file " + e);
			}
		}
	}
	
	public void resetGenerateTargets() {
		this.currentTargetNumber = 0;
	}
	
	public Target generateNextTarget() {
		if (currentTargetNumber >= NUMBER_OF_TARGETS) {
			System.out.println("ERROR: Your tried to generate more targets than the maximum " +
		                       "number of targets (" + NUMBER_OF_TARGETS + ")");
		}
		String imagePath = "img/standard/targets/" + currentTargetNumber + ".png";
		String asciiPath = "img/standard/targets/" + currentTargetNumber + ".txt";
		String ascii = getAscii(asciiPath);
		String name = ascii.split("\n")[0];
		String symbol = ascii.split("\n")[1];
		Target target = new Target(symbol, imagePath, name);
		currentTargetNumber++;
		return target;
	}

	@Override
	public Card getNextCard() {
		return getNextCard(-1);
	}
	
	@Override
	public Card getNextCard(int pictureNumber) {
		boolean[] ways = generateWays();
		if (pictureNumber == -1) {
			Random random = new Random();
			pictureNumber = random.nextInt(NUMBER_OF_CARD_IMAGES);
		}
		String imagePath = "img/standard/cards/" + pictureNumber + ".png";
		String asciiPath = "img/standard/cards/" + pictureNumber + ".txt";
		String ascii = getAscii(asciiPath);
		String[] lines = ascii.split("\n");
		String nextRotationNumber = lines[0];
		// remove first line of string which only shows next rotation position
		String symbol = ascii.substring(ascii.indexOf('\n') + 1);
		Card card = new Card(imagePath, null, ways[0], ways[1], ways[2], ways[3]);
		card.setAsciiArt(symbol, Integer.parseInt(nextRotationNumber));
		card.addObserver(observer);
		return card;
	}
	
	private void spreadTargetsOnGrid() {
		Random random = new Random();
		int row = 1;
		int col = 1;
		this.currentTargetNumber = 0;
		for (int i = 0; i < NUMBER_OF_TARGETS; i++) {
			while (true) {
				col = random.nextInt(grid.getSize()) + 1;
				row = random.nextInt(grid.getSize()) + 1;
				// generate new random coordinates if a target was set to this
				// card already
				if (grid.getCell(col, row ).getTarget() == null) {
					break;
				}
			}
			Card card = grid.getCell(col, row);
			card.setTarget(generateNextTarget());
		}
	}
}
