package de.htwg.labyrinth.models;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;




import de.htwg.util.observer.IObserver;

public class Game implements IGame {
	private Grid grid;
	private Card spareCard;
	private int numberOfPlayers = 0;
	
	private Map<Player, Card> playerNumber = new HashMap<>();
	private Map<String, Player> playerMap = new HashMap<>();

	private GridCreateStrategyTemplate createStrategy;
	private int numberOfPlayer = 0;
	
	private Player currentPlayer;
	
	private static final int NUMBER_OF_TARGETS_PER_PLAYER = 2;

	
	public Game(int gridSize, IObserver observer) {
		// Init CreateStrategyPattern
		createStrategy = AbstractGridCreateStrategyFactory.getFactory().getInstance(observer);
		this.grid = new Grid(createStrategy, gridSize);
		create();
	}
	
	public Game(IObserver observer) {
		// Init CreateStrategyPattern
		createStrategy = AbstractGridCreateStrategyFactory.getFactory().getInstance(observer);
		this.grid = new Grid(createStrategy);
		create();
	}
	
	public void removePlayers() {
		playerNumber.clear();
		playerMap.clear();
		numberOfPlayers = 0;
		numberOfPlayer = 0;
		this.currentPlayer = null;
	}
	
	public void finishTurn() {
		Map<String, Player> players = null;
		try {
			players = getPlayers();
		} catch (IllegalAccessException e) {
			return;
		}
		Iterator<Entry<String, Player>> it = players.entrySet().iterator();
		while (it.hasNext()) {
		    Player nextPlayer = it.next().getValue();
		    if (nextPlayer == currentPlayer) {
		    	if (!it.hasNext()) {
		    		// if end of player list was reached, get first player again
		    		nextPlayer = players.get(players.keySet().iterator().next());
		    	} else {
		    		nextPlayer = it.next().getValue();
		    	}
		    	currentPlayer = nextPlayer;
		    }
		}
	}
	
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}
	
	@Override
	public Grid getGrid() {
		return this.grid;
	}
	
	@Override
	public void addPlayer(Player player) {
		if (playerMap.get(player.getName()) != null) {
			throw new IllegalArgumentException("Player '" + player.getName() + "' exists already.");
		}
		playerMap.put(player.getName(), player);
		if (this.currentPlayer == null) {
			this.currentPlayer = player;
		}
	}
	
	public void addPlayerToGrid(Player player) {
		switch(numberOfPlayer) {
			case 0:
				Card card1 = grid.getCell(1, 1);
				card1.setPlayer(player);
				playerNumber.put(player, card1);
				numberOfPlayer++;
				break;
			case 1:
				Card card2 = grid.getCell(grid.getSize(), 1);
				card2.setPlayer(player);
				playerNumber.put(player, card2);
				numberOfPlayer++;
				break;
			case 2:
				Card card3 = grid.getCell(grid.getSize(), grid.getSize());
				card3.setPlayer(player);
				playerNumber.put(player, card3);
				numberOfPlayer++;
				break;
			case 3:
				Card card4 = grid.getCell(1, grid.getSize());
				card4.setPlayer(player);
				playerNumber.put(player, card4);
				numberOfPlayer++;
				break;
			default:
				throw new IllegalArgumentException("Too many or too few players!");
		}
	}
	
	public void updatePlayer(Player player, Card card) {
		Card card2 = playerNumber.put(player, card);
		card2.removePlayer(player);
		card.setPlayer(player);
	}
	
	@Override
	public Map<String, Player> getPlayers() throws IllegalAccessException {
		if (playerMap.size() == 0) {
			throw new IllegalAccessException("Please add players before you can get a list of them!");
		}
		return this.playerMap;
	}

	public void create() {
		grid.create();
		this.playerMap.clear();
		this.playerNumber.clear();
		this.spareCard = createStrategy.getNextCard();
	}
	
	public GridCreateStrategyTemplate getFactory() {
		return createStrategy;
	}

	public Card getSpareCard() {
		return spareCard;
	}
	
	public Player hasFinished() {
		for (Player player:playerMap.values()) {
			if (player.getTargets().size() == 0) {
				return player;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Player player:playerMap.values()) {
			if (player.getTargets().size() != 0) {
				sb.append(player.getName());
				sb.append(" needs to reach the target '");
				sb.append(player.getTargets().iterator().next().getAsciiArt());
				sb.append("'\n");
			}
		}
		sb.append(getSpareCard().toString());
		sb.append("\n� � � � � � � � � � �\n");
		sb.append(getGrid().toString());
		return sb.toString();
	}

	public void setSpareCard(Card spareCard) {
		this.spareCard = spareCard;
	}

	public void setNumberOfPlayers(int number) {
		this.numberOfPlayers  = number;
	}
	
	public int getNumberOfPlayers() {
		return this.numberOfPlayers;
	}
	
	public void initPlayerTargets() {
		getFactory().resetGenerateTargets();
		for (Player player:playerMap.values()) {
			for (int i = 0; i < NUMBER_OF_TARGETS_PER_PLAYER; i++) {
				player.addTarget(getNextTarget());
			}
		}
	}
	
	public Target getNextTarget() {
		Target target = getFactory().generateNextTarget();
		return target;
	}
}
