package de.htwg.labyrinth.models;

public interface ITarget {
	
	String getAsciiArt();
	
	String getImagePath();
	
	String getName();

}
