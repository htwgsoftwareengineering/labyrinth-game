package de.htwg.labyrinth.models;

public class Target implements ITarget {

	private String imagePath;
	private String asciiArt;
	private String zielName;
	
	public Target(String asciiArt, String imagePath, String zielName) {
		this.setImagePath(imagePath);
		this.setAsciiArt(asciiArt);
		this.setName(zielName);
	}
	
	private void setAsciiArt(String ascii) {
		this.asciiArt = ascii;
	}
	
	@Override
	public String getAsciiArt() {
		return this.asciiArt;
	}
	
	private void setImagePath(String img) {
		this.imagePath = img;
	}
	
	@Override
	public String getImagePath() {
		return this.imagePath;
	}
	
	private void setName(String name) {
		this.zielName = name;
	}
	
	@Override
	public String getName() {
		return this.zielName;
	}
	
	@Override
	public boolean equals(Object obj) {
		Target t = (Target) obj;
		if (!(getName().equals(t.getName()))) {
			return false;
		}
		if (!imagePath.equals(t.getImagePath())) {
			return false;
		}	
		if (!asciiArt.equals(t.getAsciiArt())) {
			return false;
		}
		return true;
	}
}
