package de.htwg.labyrinth.models;

public class Token implements IToken {
	
	private int x;
	private int y;
	
	public Token(int x, int y) {
		this.x = x;
		this.y = y;
		this.setPosition(x, y);
	}
	
	@Override
	public final void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public int[] getPosition() {
		return new int[]{x, y}; 
	}
	
	
}
