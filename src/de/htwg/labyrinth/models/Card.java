package de.htwg.labyrinth.models;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import de.htwg.util.observer.Observable;


public class Card extends Observable implements ICard{

	private static final int RIGHT_ANGLE = 90;
	private static final int THREE_QUARTER = 270;
	
	private int nextRotationNumber = 0;
	private int rotation = 0;
	private String imagePath = "";
	private Target target;
	private Player player = null;
	private Map<String, Boolean> wayMap = new HashMap<>();
	private Map<String, Boolean> resetwayMap = new HashMap<>();
	private String asciiArt = "ASCII art not set";
	public Card(String imagePath,
				Target target,
				boolean top,
				boolean bottom,
				boolean left,
				boolean right) {
		rotation = 0;
		this.setImagePath(imagePath);
		this.setTarget(target);
		this.setWay(top, bottom, left, right);
	}
	
	@Override
	public void rotate() {
		resetwayMap = this.getWay();
		if(this.rotation == (THREE_QUARTER)) {
			this.rotation = 0;
			updateWays();
		} else {
			this.rotation += RIGHT_ANGLE;
			updateWays();
		}
		this.notifyObservers();
	}
	
	@Override
	public void updateWays() {
		Map<String, String> helper = new HashMap<>();
		helper.put("left", "top");
		helper.put("top", "right");
		helper.put("right", "bottom");
		helper.put("bottom", "left");
		wayMap = this.getWay();
		for(String key : wayMap.keySet()) {
			boolean tmp = wayMap.get(key);
			wayMap.put(helper.get(key), tmp);
		}
	}
	
	@Override
	public int getAsciiSize() {
		String asciistring = this.getAsciiArt();
		String[] segs = asciistring.split("\n");
		int strrows = segs[0].length();	
		return strrows;
	}
	
	@Override
	public void resetRotation() {
		wayMap = this.resetwayMap;
		this.rotation = 0;
	}
	
	@Override
	public int getRotation() {
		return this.rotation;
	}
	
	@Override
	public final void setAsciiArt(String asciiArt, int nextRotationNumber) {
		this.asciiArt = asciiArt;
		this.nextRotationNumber = nextRotationNumber;
	}
	
	@Override
	public final int getNextRotationNumber() {
		return this.nextRotationNumber;
	}
	
	@Override
	public String getAsciiArt() {
		return asciiArt;
	}
	
	@Override
	public final void setImagePath(String img) {
		this.imagePath = img;
	}
	
	@Override
	public String getImagePath() {
		return this.imagePath;
	}
	
	@Override
	public final void setTarget(Target target) {
		this.target = target;
	}
	
	@Override
	public Target getTarget() {
		return this.target;		
	}
	
	@Override
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Override
	public Player getPlayer() {
		return this.player;
	}
	
	@Override
	public void removePlayer(Player player) {
		this.player = null;
	}
	
	
	@Override
	public final void setWay(boolean top, boolean bottom, boolean left, boolean right) {		
		wayMap.put("top", top);
		wayMap.put("bottom", bottom);
		wayMap.put("left", left);
		wayMap.put("right", right);
	}
	
	@Override
	public Map<String, Boolean> getWay() {
		return this.wayMap;
	}
	
	@Override
	public boolean equals(Object obj) {
		Card c = (Card) obj;
		if (!(rotation == c.getRotation())) {
			return false;
		}
		if (!imagePath.equals(c.getImagePath())) {
			return false;
		}	
		if (!target.equals(c.getTarget())) {
			return false;
		}	
		if (!wayMap.equals(c.getWay())) {
			return false;
		}	
		if (!asciiArt.equals(c.getAsciiArt())) {
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += rotation;
		hash += imagePath.hashCode();
		hash += target.hashCode();
		hash += wayMap.hashCode();
		hash += asciiArt.hashCode();		
		return hash;
	}
	
	public StringBuilder setPlayerToGrid(Color color, StringBuilder newAscii) {
		if(color == Color.red) {
			newAscii.setCharAt((this.getAsciiSize() * 2) - 1, 'R');
		} else if(color == Color.blue) {
			newAscii.setCharAt((this.getAsciiSize() * 2) - 1, 'B');
		} else if(color == Color.green) {
			newAscii.setCharAt((this.getAsciiSize() * 2) - 1, 'G');
		} else if(color == Color.yellow) {
			newAscii.setCharAt((this.getAsciiSize() * 2) - 1, 'Y');
		}
		return newAscii;
	}
	
	@Override
	public String toString() {		
		Target target = this.getTarget();
		Player player = this.getPlayer(); 
		if(target == null && player == null) {
			return this.getAsciiArt();
		}
		StringBuilder newAscii = new StringBuilder(asciiArt);
		if(target != null && player == null) {
			char targetFirstLetter = this.getTarget().getAsciiArt().charAt(0);
			newAscii.setCharAt(this.getAsciiSize() + 2, targetFirstLetter);
		} else if(player != null && target == null) {
			Color color = this.getPlayer().getColor();
			this.setPlayerToGrid(color, newAscii);
		} else if(target != null && player != null) {
			char targetFirstLetter = this.getTarget().getAsciiArt().charAt(0);
			newAscii.setCharAt(this.getAsciiSize() + 2, targetFirstLetter);
			Color color = this.getPlayer().getColor();
			this.setPlayerToGrid(color, newAscii);
		}		
		return newAscii.toString();
	}
	

}
