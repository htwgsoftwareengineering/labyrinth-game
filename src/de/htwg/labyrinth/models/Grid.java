package de.htwg.labyrinth.models;

import java.util.Arrays;
import java.util.Collections;







import de.htwg.labyrinth.models.Card;

/**
 * @author mozzbozz
 *
 */
public class Grid implements IGrid {

	private static final int DEFAULT_GRID_SIZE = 7;
	private static final int MIN_GRID_SIZE = 5;
	private static final int MAX_GRID_SIZE = 11;
	
	// side length of grid
	private int size;
	private Card[][] grid;
	private GridCreateStrategyTemplate createStrategy;
	
	public Grid(GridCreateStrategyTemplate createStrat, int size) {
		this.setSize(size);
		//create new grid (square)
		this.grid = new Card[size][size];
		this.createStrategy = createStrat;
		
	}
	
	public Grid(GridCreateStrategyTemplate createStrat) {
		this.setSize(DEFAULT_GRID_SIZE);
		//create new grid (square)
		this.grid = new Card[size][size];
		this.createStrategy = createStrat;
	}
	
	private void setSize(int size) {
		checkSize(size);
		this.size = size;
	}
	
	private void checkSize(int size) {
		//Check that grid size is not too small or big and the size is odd 
		if (size < MIN_GRID_SIZE || size > MAX_GRID_SIZE || size % 2 == 0) {
			throw new IllegalArgumentException("Grid size must be an odd value and between 5 and 11. " + size + " given.");
		}
		return;
	}
	
	@Override
	public int getSize() {
		return this.size;
	}
	
	@Override
	public void setCell(Card card, int x, int y) {
		if (!this.cellInGrid(x, y)) {
			throw new IllegalArgumentException("Cell coordinates must be inside Grid."
					+ "E.g. top-left cell has coordinates (1,1)."
					+ "Given: (" + x + "," + y + ")");
		}
		this.grid[x-1][y-1] = card;
	}
	
	@Override
	public Card getCell(int x, int y) {
		if (!this.cellInGrid(x, y)) {
			throw new IllegalArgumentException("Cell coordinates must be inside Grid."
					+ "E.g. top-left cell has coordinates (1,1). "
					+ "Given: (" + x + "," + y + ")");
		}
		return this.grid[x-1][y-1];
	}
	
	private Card[][] getGrid() {
		return this.grid;
	}
	
	@Override
	public boolean cellInGrid(int x, int y) {
		if (x > 0 && x <= this.getSize() && y > 0 && y <= this.getSize()) {
			return true;
		}
		return false;
	}
	
	@Override
	public Card shiftRow(int row, String direction, Card newCard) throws Exception {
		if((row % 2) != 0) {
			throw new IllegalAccessException("You can't insert the card in this row");
		}
		//save selected row in array
		Card[] rowArray = new Card[this.getSize()];
		for (int i = 0; i < this.getSize(); i++) {
			rowArray[i] = this.getCell(i + 1, row);
		}
		
		Card oldCard;
		
		//rotate cards from_left / from_right
		if (direction.equals("from_left")) {
			Collections.rotate(Arrays.asList(rowArray), 1);
			oldCard = rowArray[0];
			rowArray[0] = newCard;
		} else if (direction.equals("from_right")) {
			Collections.rotate(Arrays.asList(rowArray), -1);
			oldCard = rowArray[this.getSize() - 1];
			rowArray[this.getSize() - 1] = newCard;
		} else {
			throw new IllegalArgumentException("The direction parameter must be 'from_left' or 'from_right'. "
											+ direction + " given.");
		}
		
		//set new row
		for (int i = 0; i < this.getSize(); i++) {
			this.setCell(rowArray[i], i + 1, row);
		}
		
		return oldCard;
	}
	
	@Override
	public Card shiftCol(int col, String direction, Card newCard) throws Exception {
		if((col % 2) != 0) {
			throw new Exception("Only odd numbers allowed.");
		}
		//save selected column in array
		Card[] colArray = new Card[this.getSize()];
		for (int i = 1; i <= this.getSize(); i++) {
			colArray[i - 1] = this.getCell(col, i);
		}
		
		Card oldCard;
		
		//rotate cards from_top / from_bottom
		if (direction.equals("from_top")) {
			Collections.rotate(Arrays.asList(colArray), 1);
			oldCard = colArray[0];
			colArray[0] = newCard;
		} else if (direction.equals("from_bottom")) {
			Collections.rotate(Arrays.asList(colArray), -1);
			oldCard = colArray[this.getSize() - 1];
			colArray[this.getSize() - 1] = newCard;
		} else {
			throw new IllegalArgumentException("The direction parameter must be 'from_top' or 'from_bottom'. "
											+ direction + " given.");
		}
		
		//set new row
		for (int i = 1; i <= this.getSize(); i++) {
			this.setCell(colArray[i - 1], col, i);
		}
		
		return oldCard;
	}

	public void create() {
		createStrategy.createNewGrid(this);
	}	
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();	
		result.append("Labyrinth size: " + this.getSize() + "x" + this.getSize() + "\n");
		String[] indices = new String[this.getSize() * this.getSize()];
		String asciistring = this.getCell(1, 1).toString();
		String[] segs = asciistring.split("\n");
		int strrows = this.getCell(1, 1).getAsciiSize();
		int strcols = segs.length;
		for(int i = 0; i < this.getSize(); i++) {
			int startOfSubstr = 0;
			int endOfSubstr = strrows;
			for(int j = 0; j < this.getSize(); j++) {
				String printTui = this.getCell(j + 1, i + 1).toString();
				indices[j] = printTui;
			}
			int flag = 0;
			while(flag < strcols) {
				for(int k = 0; k < this.getSize(); k++) {
					String substr = indices[k].substring(startOfSubstr, endOfSubstr);
					result.append(substr);				
				}
				flag++;
				startOfSubstr += strrows + 1;
				endOfSubstr += strrows + 1;
				if(flag == 3) {
					result.append(" " + ("[" + (i + 1) + "]"));
					if(i % 2 != 0) {
						result.append("<");
					}
				}
				result.append("\n");
			}	
		}	
		int count = 0;
		while(count < 2) {
			for(int i = 0; i < this.getSize(); i++) {
				for(int j = 0; j < ((strrows/2) - 1); j++)
				{
					result.append(" ");
				}
				if(count == 0) {
					result.append("[" + (i + 1) + "]");
				}
				else {
					if(i % 2 != 0) {
						result.append(" " + "^" + " ");
					} else {
						result.append("   ");
					}	
				}
				for(int j = 0; j < ((strrows/2) - 1); j++)
				{
					result.append(" ");
				}
			}
			result.append("\n");
			count++;
		}	
		return result.toString();
	}

	
	@Override
	public boolean equals(Object obj) {
	  Grid g = (Grid) obj;
	  if(size != g.getSize()){
		  return false;
	  }
	  for (int row = 1; row <= size; row++) {
		  for (int col = 1; col <= size; col++) {
			  System.out.println("HI");
			  if (getCell(col, row) == null ^ g.getCell(col, row) == null) {
				  return false;
			  }
			  if (!getCell(col, row).equals(g.getCell(col, row))) {
				  return false;
			  }

		  }
	  }
	  return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += size;
		hash += Arrays.deepHashCode(getGrid());
		
		return hash;
	}
}

