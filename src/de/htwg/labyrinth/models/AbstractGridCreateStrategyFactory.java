package de.htwg.labyrinth.models;

import de.htwg.util.observer.IObserver;

/**
 * Patterns: Abstract Factory
 * This is the abstract factory for the strategy to create grids
 */
public abstract class AbstractGridCreateStrategyFactory {
	private static AbstractGridCreateStrategyFactory factory = new RandomGridCreateStrategyFactory();
	public static void setFactory(AbstractGridCreateStrategyFactory fact) {
		factory = fact;
	}
	public abstract GridCreateStrategyTemplate getInstance(IObserver observer);
	public static AbstractGridCreateStrategyFactory getFactory() {
		return factory;
	}
}