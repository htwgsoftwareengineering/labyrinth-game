package de.htwg.labyrinth.models;

import de.htwg.util.observer.IObserver;

/**
 * Patterns: Abstract Factory
 * This is a concrete factory.
 * It instantiates the strategy to fill pairs of cells by random.
 */
public class RandomGridCreateStrategyFactory extends
AbstractGridCreateStrategyFactory {

	@Override
	public GridCreateStrategyTemplate getInstance(IObserver observer) {
		return new RandomGridCreateStrategy(observer);
	}
}