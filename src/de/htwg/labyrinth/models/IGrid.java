package de.htwg.labyrinth.models;

public interface IGrid {
	
	int getSize();
	
	void setCell(Card card, int x, int y);
	
	Card getCell(int x, int y);
		
	boolean cellInGrid(int x, int y);
	
	Card shiftRow(int row, String direction, Card newCard) throws Exception;
	
	Card shiftCol(int col, String direction, Card newCard) throws Exception;
	
}
