package de.htwg.labyrinth.models;

import java.util.Map;

public interface IGame {
	
	Grid getGrid();
	
	void addPlayer(Player player);
	
	Map<String, Player> getPlayers() throws IllegalAccessException;
}
