package de.htwg.labyrinth.models;

import java.awt.Color;
import java.util.Set;

public interface IPlayer {

	void setName(String name);
	
	String getName();
	
	void setColor(String c);
	
	Color getColor();
	
	void addTarget(Target target);
	
	Set<Target> getTargets();
	
	Target removeTarget(Target target);	
}
