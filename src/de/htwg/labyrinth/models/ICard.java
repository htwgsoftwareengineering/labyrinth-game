package de.htwg.labyrinth.models;

import java.util.Map;

public interface ICard {
	
	void rotate();
	
	void updateWays();
	
	int getAsciiSize();
	
	void resetRotation();
	
	int getRotation();
	
	String getAsciiArt();
	
	void setImagePath(String img);
	
	String getImagePath();
	
	void setTarget(Target target);
	
	Target getTarget();
	
	void setWay(boolean top, boolean bottom, boolean left, boolean right);
	
	Map<String, Boolean> getWay();

	void setAsciiArt(String asciiArt, int nextRotationNumber);

	int getNextRotationNumber();

	void setPlayer(Player player);

	Player getPlayer();

	void removePlayer(Player player);
	
}

