/**
 * 
 */
package de.htwg.labyrinth.models;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * @author mozzbozz
 *
 */
public class Player implements IPlayer {
	private static final int MAX_LENGTH_NAME = 3;
	
	private String name;
	private Color color;
	private Set<Target> targets = new HashSet<>();
	
	public Player(String name, String color) {
		this.setName(name);
		this.setColor(color);
	}
	
	@Override
	public final void setName(String name) {
		if (!(name.length() >= MAX_LENGTH_NAME)) {
			throw new IllegalArgumentException(
					"Player name needs to be at least 4 characters long: "
					+ name);
		}
		this.name = name;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public final void setColor(String c) {
		//Convert String color to awt.Color object
		//also see the following link:
		//http://stackoverflow.com/questions/2854043/converting-a-string-to-color-in-java
		Color col;
		try {
		    Field field = Class.forName("java.awt.Color").getField(c);
		    col = (Color)field.get(null);
		} catch (Exception e) {
		    // not defined
			col = null;
		}
		if (col == null) {
			throw new IllegalArgumentException("Chosen color '" + c + "' is not allowed"
					+ "(only give lower case letters).");
		}
		this.color = col;
		return;
	}
	
	@Override
	public Color getColor() {
		return this.color;
	}
	
	@Override
	public void addTarget(Target target) {
		if (targets.contains(target)) {
			throw new IllegalArgumentException("This target exists already: "
					+ target.toString());
		}
		targets.add(target);
		return;
	}
	
	@Override
	public Set<Target> getTargets() {
		return this.targets;
	}
	
	@Override
	public Target removeTarget(Target target) {
		boolean removed = this.targets.remove(target);
		if (!removed) {
			throw new IllegalArgumentException(
					"Tried to remove target but did not exists: " + target.toString());
		}
		return target;
	}
	
	@Override
	public boolean equals(Object obj) {
	  Player p = (Player)obj;
	  if(!this.name.equals(p.getName())) {
		  return false;
	  }
	  if(!this.color.equals(p.getColor())) {
		  return false;
	  }
	  if(!this.targets.equals(p.getTargets())) {
		  return false;
	  }
	  return true;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += getName().hashCode();
		hash += getColor().hashCode();
		if (getTargets().size() == 0) {
			hash += getTargets().hashCode();
		}
		
		return hash;
	}
}
