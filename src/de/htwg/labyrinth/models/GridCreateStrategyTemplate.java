package de.htwg.labyrinth.models;


/**
 * Patterns: Strategy and TemplateMethod
 * This class is the abstract strategy for the strategy pattern.
 * At the same time it is a template for the implementation of a strategy
 */
public abstract class GridCreateStrategyTemplate {
	
	protected Grid grid;

	public void createNewGrid(Grid grid) {
		this.grid=grid;
		fill();
	}

	public abstract void fill();
	
	public abstract Card getNextCard();
	
	public abstract Card getNextCard(int pictureNumber);

	public abstract void resetGenerateTargets();
	public abstract Target generateNextTarget();
}
