package de.htwg.labyrinth.models;

public interface IToken {
	
	void setPosition(int x, int y);
	
	int[] getPosition();

}
