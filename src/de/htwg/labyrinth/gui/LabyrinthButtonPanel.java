package de.htwg.labyrinth.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;



import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;

import de.htwg.labyrinth.controller.LabyrinthController;
import de.htwg.util.observer.Event;
import de.htwg.util.observer.IObserver;

public class LabyrinthButtonPanel extends JPanel implements ActionListener, MouseListener, IObserver {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8646805017681272699L;
	private JButton rotateButton;
	private JComboBox playerDecision;
	private paintableLabel rotatePic, gridPic;
	private LabyrinthController controller;
	private JPanel mainPanel;
	private JPanel rotatePanel;
	private JPanel outputPanel;
	private paintableLabel spareCardLabel;
	private paintableLabel [][] playingField;
	private paintableLabel playerLabel, gameLabel, statusLabel;
	
	public LabyrinthButtonPanel(LabyrinthController c) throws IOException {
		controller = c;
		controller.addObserver(this);
		paint();
		
		outputPanel = new JPanel();
		outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.PAGE_AXIS));
		statusLabel = new paintableLabel();
		outputPanel.add(statusLabel);
		playerLabel = new paintableLabel();
		outputPanel.add(playerLabel);
		gameLabel = new paintableLabel();
		outputPanel.add(gameLabel);
		outputPanel.setPreferredSize(new Dimension(100, 100));
		outputPanel.setBackground(Color.white);
		outputPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
		mainPanel.add(outputPanel, BorderLayout.PAGE_START);
		
	}
	
	public void repaintStatus() {
		statusLabel.setText(controller.getStatus());
		String currentPlayer = controller.getCurrentPlayerName();
		if(controller.getCurrentPlayerName() != "") {
			playerLabel.setText("It's " + currentPlayer + "'s turn.");			
		}	
		String targetname = controller.getTargetNameOfCurrentPlayer();
		gameLabel.setText(targetname);
	}
	
	private ImageIcon getSpareCardImage() {
		String imagePath = controller.getSpareCardImagePath();
		ImageIcon spareCardPic = new ImageIcon(imagePath); // load the image to a imageIcon
		Image image = spareCardPic.getImage(); // transform it 
		Image newimg = image.getScaledInstance(90, 90,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		spareCardPic = new ImageIcon(newimg);  // transform it back
		return spareCardPic;
	}
	
	private ImageIcon getCardImage(int col, int row) {
		String imagePath = controller.getCardImagePath(col, row);
		ImageIcon cardPic = new ImageIcon(imagePath); // load the image to a imageIcon
		Image image = cardPic.getImage(); // transform it 
		Image newimg = image.getScaledInstance(90, 90,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		cardPic = new ImageIcon(newimg);  // transform it back
		return cardPic;
	}
	
	private void paint() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());		

		// create rotate panel
		rotatePanel = new JPanel();
		rotatePanel.setLayout(new BoxLayout(rotatePanel, BoxLayout.Y_AXIS));
		spareCardLabel = new paintableLabel();
		rotatePanel.add(spareCardLabel);		
		rotatePanel.setBackground(Color.white);
		spareCardLabel.setIcon(getSpareCardImage());
		
		rotateButton = new JButton("rotate");
		rotatePanel.add(rotateButton);
		mainPanel.add(rotatePanel, BorderLayout.LINE_START);
				
		
		//Panel for Grid
		JPanel gridPanel = new JPanel();
		int gridSize = controller.getGridSize();
		// +1 in size for additional buttons to insert the spare card
		playingField = new paintableLabel[gridSize + 2][gridSize + 2];
		gridPanel.setLayout(new GridLayout(gridSize + 2, gridSize + 2));
		gridPanel.setBackground(Color.white);
		for(int row = 0; row <= gridSize + 1; row++) {
			for(int col = 0; col <= gridSize + 1; col++) {
				playingField[col][row] = new paintableLabel();
				gridPanel.add(playingField[col][row]);
				if (!(row == 0 || col == 0)
				    && !(row > gridSize || col > gridSize)) {
					playingField[col][row].setIcon(getCardImage(col, row));
					playingField[col][row].setName(col + ":" + row);
					playingField[col][row].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					playingField[col][row].addMouseListener(this); 
				} else {
					// here we are talking about the buttons to insert the spare card
					if ((row == gridSize + 1 && col % 2 == 0 && col != gridSize + 1 && col != 0)
					     || (col == gridSize + 1 && row % 2 == 0 && row != gridSize + 1 && row != 0)
					     || (row == 0 && col % 2 == 0 && col != 0 && col != gridSize + 1)
					     || (col == 0 && row % 2 == 0 && row != 0 && row != gridSize + 1)) {
						playingField[col][row].setName(col + ":" + row);
						playingField[col][row].setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						playingField[col][row].setOpaque(true);
						playingField[col][row].setBackground(Color.LIGHT_GRAY);
						playingField[col][row].addMouseListener(this); 
					}
				}
			}
		}
		mainPanel.add(gridPanel, BorderLayout.CENTER);
		
		
		this.add(mainPanel);
		rotateButton.addActionListener(this);
	}
	
	public void repaintGrid() {
		int gridSize = controller.getGridSize();
		for(int row = 1; row <= gridSize; row++) {
			for(int col = 1; col <= gridSize; col++) {
				playingField[col][row].setIcon(getCardImage(col, row));
				Color c = controller.getPlayerColorOnCard(col, row);
				String targetPath = controller.getTargetPathOnCard(col, row);
				if (targetPath != null) {
					playingField[col][row].setTarget(targetPath);
				} else {
					playingField[col][row].removeTarget();
				}
				if (c != null) {
					playingField[col][row].setToken(true, c);
					playingField[col][row].repaint();
				} else {
					playingField[col][row].removeToken();
				}
			}
		}
	}

	public void repaintSpareCard() {
		spareCardLabel.setIcon(getSpareCardImage());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if(src == rotateButton) {
			controller.rotateSpareCard();
		}
			
	}
	
	@Override
	public void update(Event e) {
		repaintStatus();
		repaintSpareCard();
		repaintGrid();
	}
	
	
	public void mouseReleased(MouseEvent e) {  
		paintableLabel src = (paintableLabel) e.getSource();
		int col = Integer.parseInt(src.getName().split(":")[0]);
		int row = Integer.parseInt(src.getName().split(":")[1]);
		int gridSize = controller.getGridSize();
		if (row == gridSize + 1) {
			try {
				controller.fitCardIn('c', "from_bottom", col);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (col == gridSize + 1) {
			try {
				controller.fitCardIn('r', "from_right", row);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (col == 0) {
			try {
				controller.fitCardIn('r', "from_left", row);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (row == 0) {
			try {
				controller.fitCardIn('c', "from_top", col);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else {
			controller.setPlayerTo(col, row);
			controller.finishTurn();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		paintableLabel src = (paintableLabel) e.getSource();
		int col = Integer.parseInt(src.getName().split(":")[0]);
		int row = Integer.parseInt(src.getName().split(":")[1]);
		if (col == 0 || row == 0 || col == controller.getGridSize() + 1 || row == controller.getGridSize() + 1) {
			return;
		}
		playingField[col][row].setTmpToken(controller.getCurrentPlayerColor());
		playingField[col][row].repaint();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		paintableLabel src = (paintableLabel) e.getSource();
		int col = Integer.parseInt(src.getName().split(":")[0]);
		int row = Integer.parseInt(src.getName().split(":")[1]);
		playingField[col][row].revertToOrigninalToken();
		playingField[col][row].repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}  

}
