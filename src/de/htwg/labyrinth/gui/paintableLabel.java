package de.htwg.labyrinth.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

public class paintableLabel extends JLabel {
	private static final long serialVersionUID = -6064084678492514441L;
	private boolean setToken = false;
	private boolean tmpSet = false;
	private boolean stateSet = false;
	
	private Color tokenColor;
	private Color tmpColor;
	
	private boolean targetSet = false;
	private String targetFilePath = "";
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		if (targetSet) {
			File f = new File(this.targetFilePath);
			BufferedImage image = null;
			try {
				image = ImageIO.read(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
			g.drawImage(image, 2, 2, 50, 50, this);
		}
		if (setToken) {
			g2d.setColor(this.tokenColor);
			g2d.fillOval(this.getHeight() / 2 - 10, this.getWidth() / 2 - 10, 20, 20);
		}
    }
	
	public void setToken(boolean set, Color c) {
		this.setToken = set;
		this.tokenColor = c;
		this.stateSet = true;
	}
	
	public void setTmpToken(Color c) {
		if (this.stateSet == true) {
			return;
		}
		this.tmpSet = this.setToken;
	    this.setToken = true;
	    this.tmpColor = this.tokenColor;
	    this.tokenColor = c;
	}
	
	
	public void revertToOrigninalToken() {
		if (this.stateSet == true) {
			return;
		}
		this.setToken = this.tmpSet;
		this.tokenColor = this.tmpColor;
	}
	
	public void removeToken() {
		this.setToken = false;
		this.stateSet = false;
	}
	
	public void setTarget(String target) {
		this.targetSet = true;
		this.targetFilePath = target;
	}
	
	public void removeTarget() {
		this.targetSet = false;
		this.targetFilePath = "";
	}
}
