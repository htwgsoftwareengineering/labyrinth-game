package de.htwg.labyrinth.gui;

import java.io.IOException;

import javax.swing.*;

import de.htwg.labyrinth.controller.LabyrinthController;
import de.htwg.util.observer.Event;
import de.htwg.util.observer.IObserver;

public class LabyrinthFrame extends JFrame implements IObserver  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -34625476039887368L;
	private LabyrinthController controller;
	
	public LabyrinthFrame(final LabyrinthController c) throws IOException {
		controller = c;
		controller.addObserver(this);
		
		JPanel mainPanel = new JPanel();
		this.setJMenuBar(new LabyrinthMenuBar(controller));
		mainPanel.add(new LabyrinthButtonPanel(controller));
		this.setContentPane(mainPanel);
		
		this.setTitle("SE2014SS Labyrinth-game");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);

	}

	@Override
	public void update(Event e) {
		repaint();
	}

}
