package de.htwg.labyrinth.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

import de.htwg.labyrinth.controller.LabyrinthController;

public class LabyrinthMenuBar extends JMenuBar implements ActionListener {
	private JMenuItem close;
	private JMenuItem size5;
	private JMenuItem size7;
	private JMenuItem size9;
	private JMenuItem size11;
	private LabyrinthController controller;

	public LabyrinthMenuBar(LabyrinthController c) {
		controller = c;
		JMenu menu = new JMenu("Datei");
	
		menu.setMnemonic(KeyEvent.VK_T);
		this.add(menu);
		
		close = new JMenuItem("Exit");
		
		close.setMnemonic(KeyEvent.VK_T);
		
		menu.add(close);
		
		close.addActionListener(this);
		
		menu.addSeparator();
		JMenu submenu = new JMenu("create Game");
		submenu.setMnemonic(KeyEvent.VK_S);
		size5 = new JMenuItem("Grid size 5");
		submenu.add(size5);
		size7 = new JMenuItem("Grid size 7");
		submenu.add(size7);
		size9 = new JMenuItem("Grid size 9");
		submenu.add(size9);
		size11 = new JMenuItem("Grid size 11");
		submenu.add(size11);
		
		menu.add(submenu);
		
		size5.addActionListener(this);
		size7.addActionListener(this);
		size9.addActionListener(this);
		size11.addActionListener(this);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if(src == close) {
			int optionPane = JOptionPane
					.showConfirmDialog(
							this,
							"Are you sure to exit the programm?\n Delete the current score!",
							"Beenden", JOptionPane.YES_NO_OPTION);
			if (optionPane == JOptionPane.YES_OPTION) {
				System.exit(0);
			}		
		} else if(src == size5) {
			controller.setGridSize(5);
			playerOptionPane();
		} else if(src == size7) {
			controller.setGridSize(7);
			playerOptionPane();
		} else if(src == size9) {
			controller.setGridSize(9);
			playerOptionPane();
		} else if(src == size11) {
			controller.setGridSize(11);
			playerOptionPane();
		}		
	}
	
	
	public void playerOptionPane() {
		String[] choices = { "2 player", "3 player", "4 player"};
		String input = (String) JOptionPane.showInputDialog(null, "Choose now ...",
		        "Choose number of player", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
		if(input.contains("2 player")) {
			controller.setNumberOfPlayers(2);
			playerNameColor(2);
		} else if(input.contains("3 player")) {
			controller.setNumberOfPlayers(3);
			playerNameColor(3);
		} else if(input.contains("4 player")) {
			controller.setNumberOfPlayers(4);
			playerNameColor(4);
		}		
		
	}	
	
	public void playerNameColor(int numberOfPlayers) {
		int i = 0;
		while(i < numberOfPlayers) {
			String playerName = JOptionPane.showInputDialog("Enter your name:");
			String playerColor = JOptionPane.showInputDialog("Enter your color:\n[red, green, yellow, blue]");
			controller.createPlayer(playerName, playerColor);
			i++;
		}	
	}

}
