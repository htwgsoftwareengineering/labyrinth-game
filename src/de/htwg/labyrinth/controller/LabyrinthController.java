/**
 * 
 */
package de.htwg.labyrinth.controller;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import de.htwg.labyrinth.models.Card;
import de.htwg.labyrinth.models.Game;
import de.htwg.labyrinth.models.Player;
import de.htwg.labyrinth.models.Target;
import de.htwg.util.observer.Event;
import de.htwg.util.observer.IObserver;
import de.htwg.util.observer.Observable;

/**
 *
 */
public class LabyrinthController extends Observable implements IObserver{
	private static final int MAX_NUMBER_OF_PLAYERS = 4;
	private int numberOfPlayers = 1;
	private Set<String> names = new HashSet<>();
	private Set<Color> colors = new HashSet<>();
	private Game game;
	private String statusLine = "Welcome to HTWG Labyrinth!";
	
	public LabyrinthController() {
		game = new Game(this);
	}
	
	public LabyrinthController(int size) {
		game = new Game(size, this);
	}
	
	public void setGridSize(int gridSize) {
		game = new Game(gridSize, this);
		this.notifyObservers();
	}
	
	public void finishTurn() {
		game.finishTurn();
		statusLine = "Finished turn.";
		this.notifyObservers();
	}
	
	public Color getPlayerColorOnCard(int col, int row) {
		Player p = game.getGrid().getCell(col, row).getPlayer();
		if (p == null) {
			return null;
		} else {
			return p.getColor();
		}
	}
	
	public String getTargetNameOfCurrentPlayer() {
		if (getNumberOfPlayers() == 0) {
			return null;
		}
		String playerName = getCurrentPlayerName();
		try {
			for(Player p:game.getPlayers().values()) {
				if (p.getName().equals(playerName)) {
					if (p.getTargets().size() == 0) {
						return null;
					}
					Target t = p.getTargets().iterator().next();
					return t.getName();
				}
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getTargetPathOnCard(int col, int row) {
		Target t = game.getGrid().getCell(col, row).getTarget();
		if (t == null) {
			return null;
		} else {
			return t.getImagePath();
		}
	}
	
	public Color getCurrentPlayerColor() {
		if (game.getCurrentPlayer() == null) {
			return Color.BLACK;
		} else {
			return game.getCurrentPlayer().getColor();
		}
	}
	
	public String getCurrentPlayerName() {
		if (game.getCurrentPlayer() == null) {
			return "";
		} else {
			return game.getCurrentPlayer().getName();
		}
	}
	
	public void removePlayers() {
		numberOfPlayers = 1;
		names.clear();
		colors.clear();
		game.removePlayers();
	}
	
	public Player createPlayer(String name, String color) {
		if (names.contains(name)) {
			throw new IllegalArgumentException(
					"Player with this name exists already: " + name);
		}
		
		if (numberOfPlayers > MAX_NUMBER_OF_PLAYERS) {
			throw new IllegalArgumentException("Cannot create more than 4 player.");
		}

		Player player = new Player(name, color);
		Color c = player.getColor();

		if (colors.contains(c)) {
			throw new IllegalArgumentException(
					"Player with this color exists already.");
		}
		numberOfPlayers++;
		names.add(name);
		colors.add(c);
		game.addPlayerToGrid(player);
		game.addPlayer(player);
		// after all players have been generated, init targets for players
		try {
			if (game.getNumberOfPlayers() == game.getPlayers().size()) {
				game.initPlayerTargets();
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		}
		notifyObservers();
		return player;
	}
	
	public void setNumberOfPlayers(int number) {
		if(number == 1) {
			throw new IllegalArgumentException("Too few Player\n");
		}
		game.setNumberOfPlayers(number);
	}
	
	public int getNumberOfPlayers() {
		return game.getNumberOfPlayers();
	}
	
	public void create() {
		game.create();
		statusLine = "New Labyrinth created";
		notifyObservers();
	}
	
	public String getStatus() {
		return statusLine;
	}
	
	public String getGameString() {
		return game.toString();
	}
	
	public int getGridSize() {
		return game.getGrid().getSize();
	}

	public void fitCardIn(char rowcol, String direction, int number) throws Exception {
		switch (rowcol) {
		case 'r':
			game.setSpareCard(game.getGrid().shiftRow(number, direction, game.getSpareCard()));
			break;
		case 'c':
			game.setSpareCard(game.getGrid().shiftCol(number, direction, game.getSpareCard()));
			break;
		default:
			throw new IllegalArgumentException("'rowcol' parameter can only be a char 'r' or 'c', " +
										       rowcol + " given!");
		}
		statusLine = "Card was set.";
		this.notifyObservers();
	}

	public void rotateSpareCard() {
		game.getSpareCard().rotate();
		statusLine = "Rotated spare card.";
		this.notifyObservers();
	}
	
	public String getSpareCardImagePath() {
		return game.getSpareCard().getImagePath();
	}
	
	@Override
	public void update(Event e) {
		int pictureNumber = this.game.getSpareCard().getNextRotationNumber();
		Card rotatedCard = this.game.getFactory().getNextCard(pictureNumber);
		this.game.setSpareCard(rotatedCard);
	}

	public boolean setPlayerTo(int x, int y) {
		game.updatePlayer(game.getCurrentPlayer(), game.getGrid().getCell(x, y));
		if (game.getGrid().getCell(x, y).getTarget() == null) {
			return true;
		}
		if (game.getGrid().getCell(x, y).getTarget().equals(game.getCurrentPlayer().getTargets().iterator().next())) {
			// if player reached his target, remove it
			Target oldTarget = game.getCurrentPlayer().getTargets().iterator().next();
			game.getCurrentPlayer().removeTarget(oldTarget);
			game.getGrid().getCell(x, y).setTarget(null);
		}
		Player p = game.hasFinished();
		if (p != null) {
			statusLine = "Player " + p.getName() + " won!";
			java.awt.Toolkit.getDefaultToolkit().beep();
			this.notifyObservers();
			return false;
		}
		return true;
	}
	
	public int getSize() {
		return game.getGrid().getSize();
	}
	
	public String getCardImagePath(int x, int y) {
		return game.getGrid().getCell(x, y).getImagePath();
	}
}
