package de.htwg.labyrinth.tui;

import de.htwg.labyrinth.controller.LabyrinthController;
import de.htwg.labyrinth.models.Game;
import de.htwg.labyrinth.models.Grid;
import de.htwg.util.observer.Event;
import de.htwg.util.observer.IObserver;

public class TextUI implements IObserver {

	private LabyrinthController controller;
	Grid grid;


	public TextUI(LabyrinthController controller) {
		this.controller = controller;
		controller.addObserver(this);
	}

	@Override
	public void update(Event e) {
		printTUI();
	}

	private static int setUpState = 0;
	private static int playerNumber = 1;
	private static String tmpPlayerName;
	public boolean processInputLine(String line) {
		boolean continu = true;
		if (setUpState == 0) {
			controller.create();
			setUpState++;
		}
		if (setUpState == 1) {
			int number = Integer.parseInt(line);
			controller.setNumberOfPlayers(number);
			System.out.println("Please enter name of Player " +  playerNumber + " (at least 4 characters):");
			setUpState++;
			return true;
		}
		if (setUpState == 2) {
			tmpPlayerName = line;
			System.out.println("Please enter color of Player " +  tmpPlayerName + " [red, green, blue, yellow]:");
			setUpState++;
			return true;
		}
		if (setUpState == 3) {
			playerNumber++;
			if (playerNumber > controller.getNumberOfPlayers()) {
				setUpState++;
			} else {
				setUpState = 2;
				System.out.println("Please enter name of Player " +  playerNumber + " (at least 4 characters):");
			}
			controller.createPlayer(tmpPlayerName, line);
			if (setUpState == 4) {
				printTUI();
				setUpState++;
			}
			return true;
		}
		
		
		if (line.equalsIgnoreCase("q")) {
			continu = false;
		}
		if (line.equalsIgnoreCase("u")) {
			update(null);
		}	
		if (line.equalsIgnoreCase("n")) {
			setUpState = 0;
			controller.create();
		}

		if (line.equalsIgnoreCase("5") ||
			line.equalsIgnoreCase("7") ||
			line.equalsIgnoreCase("9") ||
			line.equalsIgnoreCase("11")) {
			controller.setGridSize(Integer.parseInt(line));
			controller.create();
		} else if (line.matches("[0-9]")) {
			System.out.println("Invalid option!");
		}
		
		if (line.matches("[0-9][0-9]*,[0-9][0-9]*")) {
			int x = Integer.parseInt(line.split(",")[0]);
			int y =  Integer.parseInt(line.split(",")[1]);
			if (controller.setPlayerTo(x, y)) {
				controller.finishTurn();
			}
		}
		
		// if the command line has the form t3 or l1 (from_top 3 or from_left 1), set the cell (1,2) to value 3
		if (line.matches("[lrtb][0-9][0-9]*")) {
			System.out.println("==========================================");

			char d = line.charAt(0);
			String direction = null;
			char rowcol = 0;
			switch (d) {
			case 'l':
				direction = "from_left";
				rowcol = 'r';
				break;
			case 'r':
				direction = "from_right";
				rowcol = 'r';
				break;
			case 't':
				direction = "from_top";
				rowcol = 'c';
				break;
			case 'b':
				direction = "from_bottom";
				rowcol = 'c';
				break;
			} 
			
			// Replace all non-digit with blank: the remaining string contains only digits.
			int number = Integer.parseInt(line.replaceAll("[\\D]", ""));

			try {
				controller.fitCardIn(rowcol, direction, number);
			} catch (Exception e) {
				System.out.printf("Can't insert in %d%n", number);
			}
		}
		
		if (line.matches("[r]")) {
			controller.rotateSpareCard();
		}
		return continu;
	}

	public void printTUI() {
		if (controller.getStatus().contains("won!")) {
			System.out.println(controller.getStatus() + "\n");
			controller.removePlayers();
			setUpState = 0;
			playerNumber = 1;
			System.out.println("Please enter number of Players (2-4):");
			return;
		}
		System.out.println(controller.getGameString());
		if (controller.getCurrentPlayerName() != "") {
			System.out.println("It's " + controller.getCurrentPlayerName() + "'s turn.");
		}
		System.out.println("Please enter a command("
				           + "q-quit, n-new, u-update, [lrtb][row or column]-push in card here), x,y-set player to these coordinates:");
	}

}
