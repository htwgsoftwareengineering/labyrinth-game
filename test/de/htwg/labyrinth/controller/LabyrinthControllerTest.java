/**
 * 
 */
package de.htwg.labyrinth.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.models.Game;
import de.htwg.labyrinth.models.Player;

/**
 * @author mozzbozz
 *
 */
public class LabyrinthControllerTest {
	private LabyrinthController controller1;
	private Player player1, player2;
	private Game game1;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		controller1 = new LabyrinthController();
		game1 = new Game(controller1);
		player1 = controller1.createPlayer("Hans", "red");
		player2 = controller1.createPlayer("Willi", "blue");
	}

	/**
	 * Test method for {@link de.htwg.labyrinth.controller.LabyrinthController#createPlayer(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testCreatePlayer() {
		Player player3 = new Player("Hans", "red");
		assertEquals(player3, player1);
	}

	
	@Test(expected = IllegalArgumentException.class)
	public void testTooManyPlayers() {
		//max. number of players is 4
		Player player3 = controller1.createPlayer("Theodor", "yellow");
		Player player4 = controller1.createPlayer("Mary", "grey");
		Player player5 = controller1.createPlayer("Richard", "black");
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testNewPlayerSameColor() {
		//test creating second player with same color but different name
		Player player3 = controller1.createPlayer("Theodor", "red");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNewPlayerSameName() {
		//test creating second player with same name but different colro
		Player player3 = controller1.createPlayer("Hans", "green");
	}
	
}
