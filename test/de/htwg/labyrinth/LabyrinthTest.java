package de.htwg.labyrinth;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.Labyrinth;
import de.htwg.labyrinth.models.AbstractGridCreateStrategyFactory;
import de.htwg.labyrinth.models.GridCreateStrategyTemplate;

public class LabyrinthTest {
	
	private Labyrinth labyrinth1;
	GridCreateStrategyTemplate createStrategy = AbstractGridCreateStrategyFactory.getFactory().getInstance(null);

	@Before
	public void setUp() throws Exception {
		this.labyrinth1 = new Labyrinth(createStrategy, 7);
	}

	@Test
	public void testMain() {
		//this would run until user enters dome values
		// Labyrinth.main(null);
	}

}
