package de.htwg.labyrinth.models;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.models.Card;
import de.htwg.labyrinth.models.Grid;
import de.htwg.labyrinth.models.Target;
import de.htwg.labyrinth.models.Token;

/**
 * @author mozzbozz
 *
 */
public class GridTest {
	private Grid grid1, grid2, grid3, grid4, grid5;
	private Target target1, target2;
	private Token token1, token2;

	private Card card1, card2, card3 , card4;
	GridCreateStrategyTemplate createStrategy = AbstractGridCreateStrategyFactory.getFactory().getInstance(null);


	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		grid1 = new Grid(createStrategy, 7);
		grid2 = new Grid(createStrategy, 7);
		grid3 = new Grid(createStrategy, 5);
		grid4 = new Grid(createStrategy);
		grid5 = new Grid(createStrategy, 5);


		target1 = new Target("test", "/img/standard/targets/1.png", "Treasure Chest");
		target2 = new Target("test1", "/img/standard/targets/2.png", "Chandelier");
		token1 = new Token(1,2);
		token1 = new Token(1,2);
		card1 = new Card("/img/standard/cards/1.png",
						 target1,
				     	 true,
					 	 true,
						 true,
						 false
						);
		card2 = new Card("/img/standard/cards/2.png",
					 	 target2,
					 	 true,
						 true,
						 true,
						 false
						);
		
		card3 = new Card("/img/standard/cards/new.png",
						 target2,
				     	 true,
					 	 true,
						 true,
						 false
						);
		

		//preset values in cells
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= 5; col++) {
				Card card = new Card("/img/standard/cards/" + col + "-" + row + ".png",
									  target1,
							     	  true,
								 	  true,
									  true,
									  false
									  );
				grid3.setCell(card, col, row);
			}
		}
		
		card4 = new Card("/img/standard/cards/new.png",
				 target2,
		     	 true,
			 	 false,
				 true,
				 true
				);


		//preset values in cells
		for (int row = 1; row <= 5; row++) {
			for (int col = 1; col <= 5; col++) {
				Card card = new Card("/img/standard/cards/" + col + "-" + row + ".png",
							  	target1,
							  	true,
							  	true,
							  	true,
							  	false
								);
				String ascii = 	"+---------+\n" +
								"|___| |___|\n" +
								"|_________|\n" +
								"|         |\n" +
								"+---------+\n";
				card.setAsciiArt(ascii, 1);
				grid5.setCell(card, col, row);
			}
		}		
	}

	
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testGridArgumentTooSmall() {
		new Grid(createStrategy, 3); //min size of grid is 5
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGridArgumentTooBig() {
		new Grid(createStrategy, 13); //max size of grid is 11
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGridArgumentIsOdd() {
		new Grid(createStrategy, 8); //grid size must be odd
	}
	
	@Test
	public void testDefaultSize() {
		assertEquals(grid4.getSize(), 7);
	}
	
	@Test
	public void testGetCell() {
		grid2.setCell(card1, 1, 1);
		assertEquals(card1, grid2.getCell(1, 1));
		grid2.setCell(card2, 1, 1);
		assertEquals(card2, grid2.getCell(1, 1));
		grid2.setCell(card2, 7, 7);
		assertEquals(card2, grid2.getCell(7, 7));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetCellArgumentTooSmall() {
		//test non existent arguments
		grid1.getCell(0, 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetCellArgumentTooBig() {
		//test non existent arguments
		grid1.getCell(8, 8);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetCellOutOfGrid() {
		grid1.setCell(card1, 0, 0);
	}

	@Test
	public void testCellInGrid() {
//		x > 0 && x <= this.getSize() && y > 0 && y <= this.getSize()
		assertEquals(grid1.cellInGrid(0, 1), false);
		assertEquals(grid1.cellInGrid(1, 0), false);
		assertEquals(grid1.cellInGrid(8, 1), false);
		assertEquals(grid1.cellInGrid(1, 8), false);
	}
	
	@Test
	public void testShiftRowFromLeft() {
		try {
			grid3.shiftRow(2, "from_left", card3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int col = 1; col <= 4; col++) {
			if (!(grid3.getCell(col + 1, 2).getImagePath().equals("/img/standard/cards/" + col + "-" + 2 + ".png"))) {
				fail();
			}
		}
		if (grid3.getCell(1, 2).getImagePath() != "/img/standard/cards/new.png") {
			fail();
		}
	}
	
	@Test
	public void testShiftRowFromRight() {
		try {
			grid3.shiftRow(2, "from_right", card3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int col = 2; col <= 5; col++) {
			if (!(grid3.getCell(col - 1, 2).getImagePath().equals("/img/standard/cards/" + col + "-" + 2 + ".png"))) {
				fail();
			}
		}
		if (grid3.getCell(5, 2).getImagePath() != "/img/standard/cards/new.png") {
			fail();
		}
	}
	
	@Test
	public void testShiftColFromTop() {
		try {
			grid3.shiftCol(2, "from_top", card3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int row = 1; row <= 4; row++) {
			if (!(grid3.getCell(2, row + 1).getImagePath().equals("/img/standard/cards/" + 2 + "-" + row + ".png"))) {
				fail();
			}
		}
		if (grid3.getCell(2, 1).getImagePath() != "/img/standard/cards/new.png") {
			fail();
		}
	}
	
	@Test
	public void testShiftColFromBottom() {
		try {
			grid3.shiftCol(2, "from_bottom", card3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int row = 1; row <= 4; row++) {
			if (!(grid3.getCell(2, row).getImagePath().equals("/img/standard/cards/" + 2 + "-" + (row + 1) + ".png"))) {
				fail();
			}
		}
		if (grid3.getCell(2, 5).getImagePath() != "/img/standard/cards/new.png") {
			fail();
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shiftRowIllegalArgument() {
		//test non existent arguments
		try {
			grid3.shiftRow(2, "foo", card3);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shiftColIllegalArgument() {
		//test non existent arguments
		try {
			grid3.shiftCol(2, "foo", card3);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testToString() {
		StringBuilder result = new StringBuilder();	
		result.append("Labyrinth size: " + this.grid5.getSize() + "x" + this.grid5.getSize() + "\n");
		String[] indices = new String[this.grid5.getSize() * this.grid5.getSize()];
		String asciistring = this.grid5.getCell(1, 1).getAsciiArt();
		String[] segs = asciistring.split("\n");
		int strrows = this.grid5.getCell(1, 1).getAsciiSize();
		int strcols = segs.length;
		for(int i = 0; i < this.grid5.getSize(); i++) {
			int startOfSubstr = 0;
			int endOfSubstr = strrows;
			for(int j = 0; j < this.grid5.getSize(); j++) {
				String printTui = this.grid5.getCell(j + 1, i + 1).getAsciiArt();
				indices[j] = printTui;
			}
			int flag = 0;
			while(flag < strcols) {
				for(int k = 0; k < this.grid5.getSize(); k++) {
					String substr = indices[k].substring(startOfSubstr, endOfSubstr);
					result.append(substr);				
				}
				flag++;
				startOfSubstr += strrows + 1;
				endOfSubstr += strrows + 1;
				if(flag == 3) {
					result.append(" " + ("[" + (i + 1) + "]"));
					if(i % 2 != 0) {
						result.append("<");
					}
				}
				result.append("\n");
			}	
		}	
		int count = 0;
		while(count < 2) {
			for(int i = 0; i < this.grid5.getSize(); i++) {
				for(int j = 0; j < ((strrows/2) - 1); j++)
				{
					result.append(" ");
				}
				if(count == 0) {
					result.append("[" + (i + 1) + "]");
				}
				else {
					if(i % 2 != 0) {
						result.append(" " + "^" + " ");
					} else {
						result.append("   ");
					}	
				}
				for(int j = 0; j < ((strrows/2) - 1); j++)
				{
					result.append(" ");
				}
			}
			result.append("\n");
			count++;
		}	
		assertEquals(result.toString(), grid5.toString());
		System.out.println(grid5.toString());
	}
	
	@Test
	public void testEquals() {
		assertEquals(grid1, grid2);
		assertThat(grid1, not(grid3));
		grid1.setCell(card1, 1, 1);
		if(grid1.equals(grid2)) {
			fail();
		}
//		assertThat(grid1, not(grid2));
	}
	
	@Test
	public void testHashCode() {
		assertEquals(grid1.hashCode(), grid2.hashCode());
		assertThat(grid1.hashCode(), not(grid3.hashCode()));
	}
}
