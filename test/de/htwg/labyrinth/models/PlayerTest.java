/**
 * 
 */
package de.htwg.labyrinth.models;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.awt.Color;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.models.Player;
import de.htwg.labyrinth.models.Target;

/**
 * @author mozzbozz
 *
 */
public class PlayerTest {
	private Player player1, player2, player3;
	private Target target1, target2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		player1 = new Player("Hans", "red");
		player2 = new Player("Willi", "blue");
		player3 = new Player("Hans", "red"); // needed to test equals method
		target1 = new Target("test", "/img/standard/targets/1.png", "Treasure Chest");
		target2 = new Target("test1", "/img/standard/targets/2.png", "Chandelier");
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void testNewPlayerShortName() {
		//test creating third player with a too short name
		Player player3 = new Player("Ha", "green");
	}

	
	@Test(expected = IllegalArgumentException.class)
	public void testNewPlayerWrongColor() {
		//test creating player with non-existent color
		Player player3 = new Player("Theodor", "skyblue");
	}

	/**
	 * Test method for {@link de.htwg.labyrinth.models.Player#getName()}.
	 */
	@Test
	public void testGetName() {
		assertEquals(player1.getName(), "Hans");
		assertEquals(player2.getName(), "Willi");
		player2.setName("Barbara");
		assertEquals(player2.getName(), "Barbara");
	}

	/**
	 * Test method for {@link de.htwg.labyrinth.models.Player#getColor()}.
	 */
	@Test
	public void testGetColor() {
		player1.setColor("green");
		assertEquals(player1.getColor(), Color.GREEN);
		assertEquals(player2.getColor(), Color.BLUE);
	}


	/**
	 * Test method for {@link de.htwg.labyrinth.models.Player#getTarget()}.
	 */
	@Test
	public void testGetTargets() {
		HashSet<Target> targets = new HashSet<>();
		targets.add(target1);
		targets.add(target2);
		player1.addTarget(target1);
		player1.addTarget(target2);
		assertEquals(targets, player1.getTargets());
	}

	/**
	 * Test method for {@link de.htwg.labyrinth.models.Player#removeTarget(Target)}.
	 */
	@Test
	public void testRemoveTarget() {
		HashSet<Target> targets = new HashSet<>();
		targets.add(target2);
		player1.addTarget(target1);
		player1.addTarget(target2);
		player1.removeTarget(target1);
		assertEquals(targets, player1.getTargets());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddExistingTarget() {
		//test adding target which was assigned already
		player1.addTarget(target1);
		player1.addTarget(target2);
		player1.addTarget(target1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveNonExistingTarget() {
		//test removing target which does not belong to player
		player1.addTarget(target1);
		player1.removeTarget(target2);
	}

	@Test
	public void testEquals() {
		player1.addTarget(target1);
		player1.addTarget(target2);
		player3.addTarget(target1);
		player3.addTarget(target2);
		assertEquals(player1, player3);
		//change name, so that equals() returns false
		player3.setName("Peter");
		assertThat(player1, not(player3));
		//reset name, so that player1 and 3 equal again
		player3.setName("Hans");
		assertEquals(player1, player3);
		//change color, so that equals() returns false
		player3.setColor("yellow");
		assertThat(player1, not(player3));
		//reset color, so that player1 and 3 equal again
		player3.setColor("red");
		assertEquals(player1, player3);
		player3.removeTarget(target1);
		assertThat(player1, not(player3));
	}
	
	@Test
	public void testHashCode() {
		player1.addTarget(target1);
		player1.addTarget(target2);
		player3.addTarget(target1);
		player3.addTarget(target2);
		assertEquals(player1.hashCode(), player3.hashCode());
		player3.removeTarget(target2);
		assertThat(player1.hashCode(), not(player3.hashCode()));
	}
}
