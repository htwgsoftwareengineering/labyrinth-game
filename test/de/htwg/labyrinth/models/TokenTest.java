/**
 * 
 */
package de.htwg.labyrinth.models;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.models.Token;

/**
 * @author mozzbozz
 * @author dennis
 *
 */
public class TokenTest {

	/**
	 * @throws java.lang.Exception
	 */
	Token token;  
	
	@Before
	public void setUp() throws Exception {
		token = new Token(1,1);
		
	}	
	/**
	 * Test method for {@link de.htwg.labyrinth.models.Token#getPosition()}.
	 */
	@Test
	public void testGetPosition() {	
		int[] testArray = new int[]{1, 1};
		Assert.assertArrayEquals(testArray, token.getPosition());
	}
}
