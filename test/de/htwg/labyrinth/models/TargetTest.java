package de.htwg.labyrinth.models;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.models.Target;

public class TargetTest {
	
	Target target;

	@Before
	public void setUp() throws Exception {
		target = new Target("Schatztruhe","/img/orig/target01.png", "Schatz");			
	}

	@Test
	public void testGetAsciiArt() {
		assertEquals("Schatztruhe", target.getAsciiArt());
	}
	
	@Test
	public void testGetImagePath() {
		assertEquals("/img/orig/target01.png", target.getImagePath());
	}
	
	@Test
	public void testGetName() {
		assertEquals("Schatz", target.getName());
	}

}
