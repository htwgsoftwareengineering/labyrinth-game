/**
 * 
 */
package de.htwg.labyrinth.models;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	private Game game1, game2; 
	private Grid grid1, grid2;
	private Player player1, player2, player3;
	GridCreateStrategyTemplate createStrategy = AbstractGridCreateStrategyFactory.getFactory().getInstance(null);

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		game1 = new Game(null);
		game2 = new Game(5, null);
		
		grid1 = new Grid(createStrategy);
		grid2 = new Grid(createStrategy, 5);
		
		player1 = new Player("Kate", "yellow");
		player2 = new Player("Tony", "red");
		player3 = new Player("Tony", "green");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddDuplicatePlayerName() {
		//test adding a second player with the same name
		game1.addPlayer(player2);
		game1.addPlayer(player3);
	}
	
	@Test(expected = IllegalAccessException.class)
	public void testGetPlayersBeforeAdd() throws IllegalAccessException {
		//test getting a Map of players before any player was added to the game
		game1.getPlayers();
	}
	
	/**
	 * Test method for {@link de.htwg.labyrinth.models.Game#getGrid()}.
	 */
	@Test
	public void testGetGrid() {
		game1.getGrid();
	}

	/**
	 * Test method for {@link de.htwg.labyrinth.models.Game#getPlayers()}.
	 * @throws IllegalAccessException 
	 */
	@Test
	public void testGetPlayers() throws IllegalAccessException {
		game1.addPlayer(player1);
		game1.addPlayer(player2);
		Map<String, Player> playerMap = new HashMap<>();
		playerMap.put("Kate", player1);
		playerMap.put("Tony", player2);
		assertEquals(game1.getPlayers(), playerMap);
	}

}
