package de.htwg.labyrinth.models;

import static org.junit.Assert.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.htwg.labyrinth.models.Card;
import de.htwg.labyrinth.models.Target;
import de.htwg.labyrinth.models.Token;

public class CardTest {
	
	Card card1, card2;
	Target target1;
	Map<String, Boolean> a = new HashMap<>();
	Token token;
	List<Token> tokenList = new LinkedList<>();

	@Before
	public void setUp() throws Exception {
		//TODO: update Target Object constructor when it is implemented
		target1 = new Target("test", "/img/standard/targets/1.png", "Treasure Chest");
		card1 = new Card("/img/standard/cards/1.png",						
						 target1,
				     	 true,
					 	 true,
						 true,
						 false						 
						);

		card1.setAsciiArt("+---------+\n" +
						  "|___| |___|\n" +
						  "|_________|\n" +
						  "|         |\n" +
						  "+---------+\n", 1);
		
		card2 = new Card("/img/standard/cards/1.png",						
				target1,
		 		true,
		 		true,
		 		true,
		 		false				 				
		);
		card2.setAsciiArt("---", 1);
	}
	
	@Test
	public void testResetRotation() {
		Map<String, Boolean> b = new HashMap<>();
		b.put("top", true);
		b.put("bottom", false);
		b.put("left", true);
		b.put("right", true);
		card1.rotate();		
		card1.resetRotation();
		assertTrue(b.equals(card1.getWay()));
		assertEquals(0, card1.getRotation());
		card1.rotate();
		card1.rotate();
		card1.resetRotation();
		assertEquals(0, card1.getRotation());
		card1.rotate();
		card1.rotate();
		card1.rotate();
		card1.resetRotation();
		assertEquals(0, card1.getRotation());
		card1.rotate();
		card1.rotate();
		card1.rotate();
		assertEquals(270, card1.getRotation());
		card1.rotate();
		assertEquals(0, card1.getRotation());
	}
	
	@Test
	public void testupdateWays() {
		Map<String, Boolean> c = new HashMap<>();
		c.put("top", true);
		c.put("bottom", false);
		c.put("left", true);
		c.put("right", true);
		card1.rotate();
		assertTrue(c.equals(card1.getWay()));
		
	}
	
	
	@Test
	public void testgetRotation() {
		card1.rotate();
		assertEquals(90, card1.getRotation());
		card1.rotate();
		assertEquals(180, card1.getRotation());
		card1.rotate();
		assertEquals(270, card1.getRotation());
		card1.rotate();
		assertEquals(0, card1.getRotation());
	}
	
	
	@Test
	public void testGetAsciiArt() {
		assertEquals("---", card1.getAsciiArt());
	}
	
	@Test
	public void testGetImagePath() {
		assertEquals("/img/standard/cards/1.png", card1.getImagePath());
	}
	
	@Test
	public void testGetTarget() {
		assertEquals(this.target1 , card1.getTarget());
	}
	
	@Test
	public void testGetWay() {
		a.put("top", true);
		a.put("bottom", true);
		a.put("left", true);
		a.put("right", false);		
		assertTrue(a.equals(card1.getWay()));
	}
	
	@Test
	public void testEquals() {

		assertTrue(card1.equals(card2));
		

		card2.rotate();
		assertThat(card2, not(card1));
		card2.resetRotation();
		
		card2.setImagePath("/png/standard/cards/1.png");
		assertThat(card2, not(card1));
		
		card2.setImagePath("/img/standard/cards/1.png");
		Target target2 = new Target("test1", "/png/standard/targets/1.png", "Treasure");		
		card2.setTarget(target2);
		assertThat(card2, not(card1));
		card2.setTarget(target1);
		
		card2.setWay(false, false, false, true);
		assertThat(card2, not(card1));
		card2.setWay(true, true, true, false);
		
		card2.setAsciiArt("a", 1);
		assertThat(card2, not(card1));
	}
	
	@Test
	public void testHashCode() {
		assertEquals(card1.hashCode(), card2.hashCode());
	}
	
	@Test 
	public void testtoString() {
		System.out.println(card1.toString());
	}
}
